CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS counts
(
    uid uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    system_name TEXT NOT NULL,
    system_count INTEGER NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT (now() at time zone 'utc')
);
