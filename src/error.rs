use serde_json::json;
use thiserror::Error;
use tide::{http::mime::JSON, Response};

#[derive(Error, Debug)]
pub enum Error {
    #[error("general io error: {0}")]
    Io(#[from] std::io::Error),
    #[error("database error: {0}")]
    Db(#[from] postgres::Error),
    #[error("json parse error: {0}")]
    Json(#[from] serde_json::error::Error),
    #[error("utf8 encoding error: {0}")]
    Utf8(#[from] std::str::Utf8Error),
    #[error("env error: {0}")]
    Env(#[from] dotenv::Error),
}

pub fn unauthenticated() -> Response {
    Response::builder(401)
        .body(json!({
            "message": "Unauthenticated"
        }))
        .header("Accept", "application/json")
        .content_type(JSON)
        .build()
}
