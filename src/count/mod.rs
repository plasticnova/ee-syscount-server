use crate::{controllers::count::CountRecordInsert, db::connection, error::Error, Config};
use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct Records {
    pub records: Vec<CountModel>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CountModel {
    pub uid: Uuid,
    pub system_name: String,
    pub system_count: i32,
    pub created_at: DateTime<Utc>,
}

pub fn insert(config: &Config, record: &CountRecordInsert) -> Result<(), Error> {
    let mut client = connection(&config)?;
    client.execute(
        "INSERT INTO counts (system_name, system_count) VALUES ($1, $2)",
        &[&record.system_name.to_uppercase(), &record.system_count],
    )?;
    Ok(())
}

pub fn list(config: &Config, system_name: Option<String>) -> Result<Records, Error> {
    let mut client = connection(&config)?;
    let mut r = Records::default();
    let q: String;
    match system_name {
        Some(name) => {
            q = format!("SELECT * from counts WHERE system_name = '{}' AND created_at <= NOW() AND created_at > (NOW() - INTERVAL '5 minutes')", name.to_uppercase());
        }
        None => {
            q = format!("SELECT * from counts WHERE created_at <= NOW() AND created_at > (NOW() - INTERVAL '5 minutes')");
        }
    }
    for row in client.query(&*q, &[])? {
        let entry = CountModel {
            uid: row.get(0),
            system_name: row.get(1),
            system_count: row.get(2),
            created_at: row.get(3),
        };
        r.records.push(entry);
    }
    Ok(r)
}

pub fn delete(config: &Config, uid: &Uuid) -> Result<(), Error> {
    let mut client = connection(&config)?;
    client.execute("DELETE FROM counts WHERE uid = $1", &[&uid])?;
    Ok(())
}

pub fn requires_cleanup(config: &Config) -> Result<Records, Error> {
    let mut client = connection(&config)?;
    let mut r = Records::default();
    for row in client.query(
        "SELECT * from counts WHERE created_at < (NOW() - INTERVAL '1 days')",
        &[],
    )? {
        let entry = CountModel {
            uid: row.get(0),
            system_name: row.get(1),
            system_count: row.get(2),
            created_at: row.get(3),
        };
        r.records.push(entry);
    }
    Ok(r)
}
