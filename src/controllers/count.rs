use crate::{count, error, State};
use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use tide::{http::mime::JSON, Request, Response};

#[derive(Deserialize, Serialize)]
pub struct CountCreateRequest {
    pub access_token: String,
    pub system_name: String,
    pub system_count: usize,
}

#[derive(Deserialize, Serialize)]
pub struct CountListRequest {
    pub access_token: String,
    pub system_name: Option<String>,
}

#[derive(Clone, Deserialize, Serialize)]
pub struct CountRecordInsert {
    pub system_name: String,
    pub system_count: i32,
}

pub async fn index(mut req: Request<State>) -> tide::Result {
    let CountListRequest {
        access_token,
        system_name,
    } = req.body_json().await?;

    let config = &req.state().config;
    if access_token != *config.access_token {
        return Ok(error::unauthenticated());
    }

    let records = count::list(&config, system_name)?;
    let response = Response::builder(200)
        .body(serde_json::to_string(&records)?)
        .content_type(JSON)
        .build();

    Ok(response)
}

pub async fn create(mut req: Request<State>) -> tide::Result {
    let CountCreateRequest {
        access_token,
        system_name,
        system_count,
    } = req.body_json().await?;

    let config = &req.state().config;
    if access_token != *config.access_token {
        return Ok(error::unauthenticated());
    }

    let record = CountRecordInsert {
        system_name,
        system_count: system_count.try_into()?,
    };

    count::insert(&config, &record)?;

    let response = Response::builder(200)
        .body(serde_json::to_string(&record)?)
        .content_type(JSON)
        .build();

    Ok(response)
}
