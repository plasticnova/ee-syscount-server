mod config;
mod controllers;
mod count;
mod db;
mod error;
mod routes;
mod tasks;

use config::Config;
use std::thread;
use tide::Server;

#[derive(Clone)]
pub struct State {
    config: Config,
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    let state = State {
        config: Config::from_env()?,
    };
    db::run_migrations(Config::from_env()?)
        .await
        .expect("Unable to run DB migrations: {}");
    thread::spawn(|| tasks::manager::start(Config::from_env()?));
    tide::log::start();
    let mut app = Server::with_state(state);
    app = routes::init(app);
    app.listen("0.0.0.0:8080").await?;
    Ok(())
}
