use crate::error::Error;
use dotenv;
use std::sync::Arc;

#[derive(Clone, Default)]
pub struct Config {
    pub access_token: Arc<String>,
    pub postgres_user: Arc<String>,
    pub postgres_pass: Arc<String>,
    pub postgres_host: Arc<String>,
    pub postgres_db: Arc<String>,
    pub postgres_port: Arc<String>,
}

impl Config {
    pub fn from_env() -> Result<Self, Error> {
        dotenv::dotenv().ok();
        let access_token = Arc::new(dotenv::var("ACCESS_TOKEN")?);
        let postgres_user = Arc::new(dotenv::var("POSTGRES_USER")?);
        let postgres_pass = Arc::new(dotenv::var("POSTGRES_PASS")?);
        let postgres_host = Arc::new(dotenv::var("POSTGRES_HOST")?);
        let postgres_db = Arc::new(dotenv::var("POSTGRES_DB")?);
        let postgres_port = Arc::new(dotenv::var("POSTGRES_PORT")?);
        Ok(Config {
            access_token,
            postgres_user,
            postgres_pass,
            postgres_host,
            postgres_db,
            postgres_port,
        })
    }
}
