use crate::{error::Error, Config};
use std::time::Duration;
mod db_cleanup;
pub mod manager;

pub use db_cleanup::DbCleanupTask;

pub trait Task {
    fn interval(&self) -> Duration;
    fn run(&self, config: Config);
    fn exec(&self, config: Config) -> Result<(), Error>;
    fn on_error(&self, config: Config);
    fn on_success(&self, config: Config);
}
