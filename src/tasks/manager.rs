use super::{DbCleanupTask, Task};
use crate::{error::Error, Config};
use std::thread;

pub fn start(config: Config) -> Result<(), Error> {
    thread::spawn(move || loop {
        DbCleanupTask::get().run(config.clone());
        thread::sleep(DbCleanupTask::get().interval());
    });
    Ok(())
}
