use super::Task;
use crate::{count, error::Error, Config};
use std::time::Duration;

#[derive(Clone)]
pub struct DbCleanupTask {}

impl DbCleanupTask {
    pub fn get() -> Self {
        DbCleanupTask {}
    }
}

impl Task for DbCleanupTask {
    fn interval(&self) -> Duration {
        Duration::from_secs(3600)
    }
    fn run(&self, config: Config) {
        match &self.exec(config.clone()) {
            Ok(_) => {
                &self.on_success(config.clone());
            }
            Err(_) => {
                &self.on_error(config.clone());
            }
        }
    }
    fn exec(&self, config: Config) -> Result<(), Error> {
        dbg!("Checking for old records");
        let r = count::requires_cleanup(&config)?;
        for record in r.records.iter() {
            count::delete(&config, &record.uid)?;
            dbg!(format!("Removed record: {}", &record.uid));
        }
        Ok(())
    }
    fn on_error(&self, _config: Config) {
        dbg!("Error cleaning records");
    }
    fn on_success(&self, _config: Config) {
        dbg!("Task completed successfully");
    }
}
