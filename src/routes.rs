use crate::{controllers, State};
use serde_json::json;
use tide::Server;

pub fn init(mut server: Server<State>) -> Server<State> {
    // Health route
    server.at("/").get(|_| async {
        Ok(json!({
            "message": "Hello World!"
        }))
    });
    // Count route
    server
        .at("/api/count")
        .get(controllers::count::index)
        .post(controllers::count::create);
    server
}
