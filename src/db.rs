use crate::{config::Config, error::Error};

use postgres::{Client, NoTls};

mod embedded {
    use refinery::embed_migrations;
    embed_migrations!("sql_migrations");
}

fn connection_string(config: &Config) -> String {
    format!(
        "postgres://{}:{}@{}:{}/{}",
        config.postgres_user,
        config.postgres_pass,
        config.postgres_host,
        config.postgres_port,
        config.postgres_db
    )
}

pub fn connection(config: &Config) -> Result<Client, Error> {
    Ok(Client::connect(&connection_string(&config), NoTls)?)
}

pub async fn run_migrations(config: Config) -> Result<(), Error> {
    println!("Running DB migrations...");
    let mut client = connection(&config)?;
    let migration_report = embedded::migrations::runner().run(&mut client).unwrap();
    for migration in migration_report.applied_migrations() {
        println!(
            "Migration Applied -  Name: {}, Version: {}",
            migration.name(),
            migration.version()
        );
    }
    println!("DB migrations finished!");
    Ok(())
}
